const express = require("express");
const app = express();

// Route for api
app.get("/api", (req, res) => {
  res.json({ users: ["userOne", "userTwo", "userThree"] });
});

app.listen(5000, () => {
  console.log("Server statred on pot 5000");
});
